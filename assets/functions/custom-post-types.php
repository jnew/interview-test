<?php
/* Custom Post Type */
  function create_post_type() {
    register_post_type( 'Testimonials', // POST TYPE NAME
      array(
        'thumbnail',
        'labels' => array(
          'name' => __( 'Testimonials' ),
          'singular_name' => __( 'testimonial' )
        ),
        'can_export'          => TRUE,
        'exclude_from_search' => TRUE,
        'publicly_queryable'  => TRUE,
        'query_var'           => 'testimonial',
        'show_ui'             => TRUE,
        'public' => true,
        'has_archive' => true,
        'menu_icon' => 'dashicons-groups',
        'supports' => array( 'title', 'editor', 'thumbnail'),
        'show_in_menu'        => TRUE
      )
    );
    register_post_type( 'Content Blocks', // POST TYPE NAME
      array(
        'thumbnail',
        'labels' => array(
          'name' => __( 'Content Blocks' ),
          'singular_name' => __( 'Content Block' )
        ),
        'can_export'          => TRUE,
        'exclude_from_search' => TRUE,
        'publicly_queryable'  => TRUE,
        'query_var'           => 'content-block',
        'show_ui'             => TRUE,
        'public' => true,
        'has_archive' => true,
        'menu_icon' => 'dashicons-format-aside',
        'supports' => array( 'title', 'editor', 'thumbnail'),
        'show_in_menu'        => TRUE
      )
    );
  }
  add_action( 'init', 'create_post_type' );
?>
