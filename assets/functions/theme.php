<?php

/* Create Wordpress Menus */
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'footer-menu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );


/* Change Wordpress Login Logo */
function my_login_logo() { ?>
    <style type="text/css">
        .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/login-logo.png);
            background-position: 0 0;
            background-size: 300px auto;
            height: 120px;
            padding-bottom: 30px;
            width: 300px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

/* Add featured image support to posts, pages etc */
add_theme_support('post-thumbnails', array(
  'post',
  'page'
));



?>