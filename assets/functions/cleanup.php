<?php
/* Remove admin bar when viewing site */
add_filter('show_admin_bar', '__return_false');

/* Remove rubbish that wordpress adds to head in source */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0 );
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action( 'wp_head', 'wp_generator');
remove_action('wp_head', 'rel_canonical');
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
function twentyten_remove_recent_comments_style() {  
  global $wp_widget_factory;  
  remove_action( 'wp_head', array(
    $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' 
  ) );  
}
add_action( 'widgets_init', 'twentyten_remove_recent_comments_style' );  

/* Stop wordpress adding jquery */
function zero_script() {
        wp_deregister_script('jquery');
}
add_action('wp_enqueue_scripts', 'zero_script');



?>