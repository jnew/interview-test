<?php
/* If is the first registered user */
$user_id = get_current_user_id();
if ($user_id == 1) {

  /* Remove pages from Wordpress admin menu */
  function remove_menus(){
    remove_menu_page( 'upload.php' );                 // Media
    remove_menu_page( 'edit-comments.php' );          // Comments
    remove_submenu_page( 'edit.php', 'edit-tags.php?taxonomy=post_tag' );    // Post tags
  }
  add_action( 'admin_menu', 'remove_menus' );

} else {

  /* Remove pages from Wordpress admin menu */
  function remove_menus(){
    remove_menu_page( 'upload.php' );                                        // Media
    remove_menu_page( 'edit-comments.php' );                                 // Comments
    remove_menu_page( 'plugins.php' );                                       // Plugins
    remove_menu_page( 'tools.php' );                                         // Tools
    remove_menu_page( 'options-general.php' );                               // Settings
    remove_submenu_page('options-general.php', 'tinymce-advanced');          // tinymce settings
    remove_submenu_page( 'edit.php', 'edit-tags.php?taxonomy=post_tag' );    // Post tags
    remove_menu_page('edit.php?post_type=acf');                              // Advanced custom fields
  }
  add_action( 'admin_menu', 'remove_menus', 999 );

/* Remove pages from Wordpress admin edit page archive */
  // function exclude_this_page( $query ) {
  //   if( !is_admin() )
  //     return $query;

  //   global $pagenow;
  //   if( 'edit.php' == $pagenow && ( get_query_var('post_type') && 'page' == get_query_var('post_type') ) )
  //     $query->set( 'post__not_in', array(7) );
  //   return $query;
  // }
  // add_action( 'pre_get_posts' ,'exclude_this_page' );

}

// Styling Admin area remove widget titles
function custom_admin_css() {
   echo '<style type="text/css">
           #latest-comments, #wp-version-message, li.comment-count, #dashboard-widgets h3.hndle, #dashboard-widgets .handlediv, #jetpack_summary_widget footer {
            display: none;
           }
           #dashboard_right_now a.post_type-count:before, #dashboard_right_now span.post_type-count:before {
            content: "\f109";
           }
           #dashboard_right_now li:last-of-type {
            margin-bottom: 0px;
           }
         </style>';
}
add_action('admin_head', 'custom_admin_css');

/* Get an excrpt of a item by ID */
function get_excerpt_by_id($post_id){
  $the_post = get_post($post_id);
  $the_excerpt = $the_post->post_content; 
  $excerpt_length = 32;
  $the_excerpt = strip_tags(strip_shortcodes($the_excerpt));
  $words = explode(' ', $the_excerpt, $excerpt_length + 1);
  if(count($words) > $excerpt_length) :
  array_pop($words);
  array_push($words, '…');
  $the_excerpt = implode(' ', $words);
  endif;
  $the_excerpt = '' . $the_excerpt . '';
  return $the_excerpt;
}

/* Remove Wordpress admin help tabs */
add_action('admin_head', 'mytheme_remove_help_tabs');
function mytheme_remove_help_tabs() { $screen = get_current_screen(); $screen->remove_help_tabs(); }

/* Change Wordpress admin footer text */
if (! function_exists('dashboard_footer') ){ 
    function dashboard_footer () { echo 'Web Design & Development by Armstrong'; }
  } 
add_filter('admin_footer_text', 'dashboard_footer');

/* Make site name link on Wordpress admin header open in new tab */
add_action( 'admin_bar_menu', 'customize_my_wp_admin_bar', 80 );
function customize_my_wp_admin_bar( $wp_admin_bar ) {
    $node = $wp_admin_bar->get_node('site-name');
    $node->meta['target'] = '_blank';
    $wp_admin_bar->add_node($node);
}

/* Remove widgets from Wordpress admin dashboard */
remove_action('welcome_panel', 'wp_welcome_panel');
function remove_dashboard_widgets() {
  global $wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

/* Remove various items in Wordpress admin and on admin header */
function remove_admin_bar_links() {
  global $wp_admin_bar;
  $wp_admin_bar->remove_menu('wp-logo');          // Remove the WordPress logo
  $wp_admin_bar->remove_menu('about');            // Remove the about WordPress link
  $wp_admin_bar->remove_menu('wporg');            // Remove the WordPress.org link
  $wp_admin_bar->remove_menu('documentation');    // Remove the WordPress documentation link
  $wp_admin_bar->remove_menu('support-forums');   // Remove the support forums link
  $wp_admin_bar->remove_menu('feedback');         // Remove the feedback link
  $wp_admin_bar->remove_menu('view-site');        // Remove the view site link
  $wp_admin_bar->remove_menu('updates');          // Remove the updates link
  $wp_admin_bar->remove_menu('comments');         // Remove the comments link
  $wp_admin_bar->remove_menu('new-content');      // Remove the content link
  $wp_admin_bar->remove_menu('w3tc');             // If you use w3 total cache remove the performance link
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

/* Remove updates from Wordpress dashboard on admin menu */
function ap_remove_menus( ) {
  remove_submenu_page( 'index.php', 'update-core.php' );
} 
add_action( 'admin_menu', 'ap_remove_menus', 999 );

/* Stop tinyMCE removing span tags */
add_filter('tiny_mce_before_init', 'tinymce_init');
function tinymce_init( $init ) {
    $init['extended_valid_elements'] .= ', span[style|id|nam|class|lang]';
    $init['verify_html'] = false;
    return $init;
}

/* Remove width and height added to img tags by wordpress upload tool */
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );
function remove_width_attribute( $html ) {
$html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
return $html;
}

?>