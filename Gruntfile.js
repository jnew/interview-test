module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		bowercopy: {
			options: {
				clean: true
			},
			scss: {
				options: {
					destPrefix: 'src/scss'
				},
				files: {
					// Foundation
					'foundation/': 'foundation-sites/scss/*',
					'slick/slick.scss':'slick-carousel/slick/slick.scss',
					'slick/slick-theme.scss':'slick-carousel/slick/slick-theme.scss',
					'fontawesome':'fontawesome/scss/*'
				}
			},
			js: {
				options: {
					destPrefix: 'src/js'
				},
				files: {
					// Foundation
					'foundation.js': 'foundation-sites/dist/foundation.min.js',
					'jquery.js': 'jquery/dist/jquery.min.js',
					//Slick
					'slick.js':'slick-carousel/slick/slick.min.js',
					//Placeholder
					'placeholder.js':'jquery-placeholder/jquery.placeholder.min.js'
				}
			},
			fonts:{
				options:{
					destPrefix: 'fonts'
				},
				files:{
					'':'slick-carousel/slick/fonts/*',
					'':'fontawesome/fonts/*'
				}
			}
		},

		sass: {
			dist: {
				options: {
					style: 'compressed',
					loadPath: ['node_modules/foundation-sites/scss'],
					sourcemap: 'none'
				},
				files: {
					'css/styles.css': 	'src/scss/styles.scss'
				}
			}
		},


		//Uglify
		uglify: {
			my_target: {
				files: {
					'js/main.min.js': [
										'src/js/jquery.js',
										'src/js/placeholder.js',
										'src/js/main.js',
										'src/js/foundation.js',
										'src/js/slick.js',
									   ]
				}
			}
		},
		imagemin: {                         
		    static: {                         
		      options: {                       
		        optimizationLevel: 1,
		      },
		    },
		    dynamic: {                       
		      files: [{
		        expand: true,                 
		        cwd: 'originalImg',                  
		        src: ['**/*.{png,jpg,gif}'],   
		        dest: 'img/'               
		      }]
		    }
		  },
		//Watch Tasks
		watch: {
			livereload: {
				options: {
					livereload: 1990
				},
				files: ['src/scss/*.scss', 'src/js/*.js','*.php','partials/*.php','templates/*.php']
			},

			grunt: {
				files: ['Gruntfile.js']
			},

			js: {
				files: 'src/js/*.js',
				tasks: ['uglify']
			},

			sass: {
				files: 'src/scss/**/*.scss',
				tasks: ['sass']
			}
		}
	});

	//grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-bowercopy');
	grunt.loadNpmTasks('grunt-contrib-imagemin');

	grunt.registerTask('build', ['sass', 'uglify','watch']);
	grunt.registerTask('default', ['build']);
	grunt.registerTask('image',['imagemin']);
}