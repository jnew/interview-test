/*-----------------------------------------*/
/*-----------------------------------------*/
/*---------- BASE MAIN JS FILE -----------*/
/*-----------------------------------------*/
/*-----------------------------------------*/

$(document).ready(function(){
//Initialize Foundation
	$(document).foundation();
	$('.tweetsSlider').slick({
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 3,
		dots:true,
		arrows:true,
		responsive: [
		 {
			 breakpoint: 1023,
			 settings: {
				 slidesToShow: 2,
				 slidesToScroll: 2
			 }
		 },
		 {
			 breakpoint: 640,
			 settings: {
				 slidesToShow: 1,
				 slidesToScroll: 1,
				 arrows:false
			 }
		 }
	 ]
	});

//Testimonials Slider
	$('.testimonialSlider').slick({
		infinite: true,
		dots:true,
		arrows:true
	});

//Search increment for plus and minus ,this should be reworked as there is code duplication
	$('.plus.incrementButton').click(function(){
		value = $('.roomsField').val();
		value.replace ( /[^\d.]/g, '' );
		newValue = parseInt(value) + 1;
		updateRooms(newValue)
	});

	$('.minus.incrementButton').click(function(){
		value = $('.roomsField').val();
		value.replace ( /[^\d.]/g, '' );
		newValue = parseInt(value) - 1;
		if(newValue > 0){
			updateRooms(newValue);
		}
	});


//Function to update rooms string
	function updateRooms(value){

		if(value < 2){
			$('.roomsField').val(value+' Room');
		}else{
			$('.roomsField').val(value+' Rooms');
		}
	}

//Housing information menu
	$('.contentMenu li a').click(function(e){
		e.preventDefault();
		index = $(this).parent().index();
		$('.contentMenu li a').removeClass('active');
		$(this).addClass('active');
		$('.contentContainer .content').removeClass('active');
		$('.contentContainer .content').eq(index).addClass('active');
		$('.contentMenu .fa').removeClass('rotateRight');
		$(this).next().addClass('rotateRight');
	});

});
