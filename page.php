<?php get_header(); ?> 

<?php /* if ( is_active_sidebar( 'sidebar' ) ) : ?>
	<?php dynamic_sidebar( 'sidebar' ); ?>
<?php endif; */ ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<section id="page" class="post-<?php the_ID(); ?>">
	<?php the_content(); ?>
</section>
<?php endwhile; else : ?>
<section>
	<h2>Not Found</h2>
	<p>Sorry, but you are looking for something that isn't here.</p>
</section>
<?php endif; ?>

<?php get_footer(); ?>