<footer>
	<ul>
		<li>&copy;<?php echo date('Y');?> The Guiness Partnership</li>
		<li>Cookies</li>
		<li>Privacy Policy</li>
		<li>Terms & Conditions</li>
		<li>Contact</li>
		<li>Sitemap</li>
		<li>Developed By Prodo Digital</li>
	</ul>
</footer>
<script src="<?php bloginfo('template_url'); ?>/js/main.min.js"></script>
<?php wp_footer() ?>
</body>
</html>
