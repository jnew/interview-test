Test

I have packaged up the test as a theme , there will need to be a base Wordpress install set up , and the theme will need to be applied.  

I have used placeholder images as the PSD didn't have extractable elements in the layers , possibly because I am using a older version of photoshop.  

Will need to do a search and replace on the database for any url with http://localhost:8888/housing to whatever the url of the Wordpress install is.  

Have split up some of the extra functions that we use in our boilerplate that has been created under assets/functions

Plugins used are:  

- Oauth twitter feed for developers  - Download here:  
https://en-gb.wordpress.org/plugins/oauth-twitter-feed-for-developers/  
Setup keys should be in the database that I have included.  

- ACF - Advanced custom fields  
https://wordpress.org/plugins/advanced-custom-fields/


Shouldn't need to run npm or bower install as all of the styles and scripts are compiled in the repository.

Any problems give me an email james.new@ntlworld.com