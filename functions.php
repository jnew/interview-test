<?php 
require_once(get_template_directory().'/assets/functions/admin-cleanup.php');
require_once(get_template_directory().'/assets/functions/cleanup.php');
require_once(get_template_directory().'/assets/functions/contact-form.php');
require_once(get_template_directory().'/assets/functions/custom-post-types.php');
require_once(get_template_directory().'/assets/functions/general.php');
require_once(get_template_directory().'/assets/functions/theme.php');
?>