<!doctype html>
<head>
	<?php
	if( in_array( $_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
		echo '<script src="//localhost:1990/livereload.js"></script>';
	}?>
	<meta charset="utf-8">
	<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1' />
	<title><?php wp_title('|',true,'right'); ?><?php bloginfo('name'); if ( is_front_page()||is_home() ) { ?> · <?php bloginfo('description'); ?><?php } ?></title>
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" type="image/x-icon">
	<?php wp_head() ?>
	<link href="<?php bloginfo('template_url'); ?>/css/styles.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<meta name="author" content="<?php bloginfo('name')?>" >
	<meta name="description" content="<?php if ( is_front_page()||is_home() ) { ?><?php bloginfo('name')?> - <?php bloginfo('description'); ?><?php } if ( is_single()||is_page() ) { $my_excerpt = get_excerpt_by_id($post_id); $desc1 = strip_tags($my_excerpt, ''); $desc2 = preg_replace( "/\r|\n/", "", $desc1 ); echo $desc2; } ?>" />
	<meta property="og:title" content="<?php wp_title('|',true,'right'); ?><?php bloginfo('name'); if ( is_front_page()||is_home() ) { ?> · <?php bloginfo('description'); ?><?php } ?>" />
	<meta property="og:url" content="<?php echo current_page_url(); ?>" />
	<meta property="og:site_name" content="<?php wp_title('|',true,'right'); ?><?php bloginfo('name'); if ( is_front_page()||is_home() ) { ?> · <?php bloginfo('description'); ?><?php } ?>" />
	<meta property="og:description" content="<?php if ( is_front_page()||is_home() ) { ?><?php bloginfo('name')?> - <?php bloginfo('description'); ?><?php } if ( is_single()||is_page() ) { $my_excerpt = get_excerpt_by_id($post_id); $desc1 = strip_tags($my_excerpt, ''); $desc2 = preg_replace( "/\r|\n/", "", $desc1 ); echo $desc2; } ?>" />
	<meta property="og:image" content="<?php if ( is_home()||is_category()||is_front_page()||is_page() ) { bloginfo('template_url'); ?>/img/share.jpg<?php } if ( is_single() ) { if ( '' != get_the_post_thumbnail() ) { $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); echo $image[0]; } else { bloginfo('template_url'); ?>/img/share.jpg<?php } } ?>" />
	<meta property="og:type" content="website" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:url" content="<?php echo current_page_url(); ?>" />
	<meta name="twitter:title" content="<?php wp_title('|',true,'right'); ?><?php bloginfo('name'); if ( is_front_page()||is_home() ) { ?> · <?php bloginfo('description'); ?><?php } ?>" />
	<meta name="twitter:description" content="<?php if ( is_front_page()||is_home() ) { ?><?php bloginfo('name')?> - <?php bloginfo('description'); ?><?php } if ( is_single()||is_page() ) { $my_excerpt = get_excerpt_by_id($post_id); $desc1 = strip_tags($my_excerpt, ''); $desc2 = preg_replace( "/\r|\n/", "", $desc1 ); echo $desc2; } ?>" />
	<meta name="twitter:image" content="<?php if ( is_home()||is_category()||is_front_page()||is_page() ) { bloginfo('template_url'); ?>/img/share.jpg<?php } if ( is_single() ) { if ( '' != get_the_post_thumbnail() ) { $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); echo $image[0]; } else { bloginfo('template_url'); ?>/img/share.jpg<?php } } ?>" />
	<meta name="robots" content="index, follow" />
</head>

<body <?php body_class($class); ?>>
<header>
	<div class="row">
		<div class="large-3 medium-3 desktopHeaderButtons text-left columns">
			<div class="headerButton">
				<i class="fa fa-navicon"></i>Menu
			</div>
			<div class="headerButton">
				<i class="fa fa-search"></i>Search
			</div>
		</div>
		<div class="large-6 medium-6 logoColumn text-center columns">
			<img src="http://lorempixel.com/130/60/city" />
		</div>
		<div class="large-3 medium-3 desktopHeaderButtons text-right columns">
			<div class="headerButton">
				<i class="fa fa-lock"></i>Login
			</div>
			<div class="headerButton">
				<i class="fa fa-pencil"></i>Register
			</div>
		</div>
	</div>
	<div class="mobileHeaderButtons">
		<ul>
			<li><i class="fa fa-navicon"></i></li>
			<li><i class="fa fa-search"></i></li>
			<li><i class="fa fa-lock"></i></li>
			<li><i class="fa fa-pencil"></i></li>
		</ul>
	</div>
</header>
