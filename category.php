<?php get_header(); ?>

<?php /* if ( is_active_sidebar( 'sidebar' ) ) : ?>
	<?php dynamic_sidebar( 'sidebar' ); ?>
<?php endif; */ ?>

<section id="category">
<?php if ( have_posts() ) : ?>
	<h1>Category: <?php single_cat_title( '', true ); ?></h1>
	<?php while ( have_posts() ) : the_post(); ?>
	<div class="post-<?php the_ID(); ?>">
		<?php if (has_post_thumbnail()) { ?>
			<img src="<?php $thumb_id = get_post_thumbnail_id(); $thumb_url = wp_get_attachment_image_src($thumb_id,'full', true); echo $thumb_url[0]; ?>" />
		<?php } ?>
		<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
		<h3>Posted on <?php the_time('l, F jS, Y') ?></h3>
		<?php the_content(); ?>
		<div class="tags">
			<?php the_tags('Tags: ',', '); ?>
		</div>
		<div class="socialLinks">
			Share: 
			<a href="http://www.facebook.com/share.php?u=<?php the_permalink() ?>" target="_blank"><i class="fa fa-facebook"></i> Facebook</a> 
			<a href="http://twitter.com/home/?status=<?php the_permalink() ?>" target="_blank"><i class="fa fa-twitter"></i> Twitter</a> 
			<a href="//www.pinterest.com/pin/create/button/?url=<?php the_permalink() ?>&media=<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $feat_image; ?>&description=<?php the_title(); ?><?php echo get_excerpt_by_id($post_id); ?>" target="_blank"><i class="fa fa-pinterest"></i> Pinterest</a> 
			<a href="https://plus.google.com/share?url=<?php the_permalink() ?>" target="_blank"><i class="fa fa-google-plus"></i> Google+</a> 
			<a href="http://www.stumbleupon.com/submit?url=<?php the_permalink() ?>" target="_blank"><i class="fa fa-stumbleupon"></i> Stumbleupon</a>
		</div>
	</div>
	<?php endwhile; ?>
	<div class="page_nav">
		<?php previous_posts_link('Newer Posts') ?>
		<?php next_posts_link('Older Posts') ?>
	</div>
<?php else: ?>
	<h2>Sorry, no <?php single_cat_title( '', true ); ?> items at this time.</h2>
<?php endif; ?>
</section>

<?php get_footer(); ?>