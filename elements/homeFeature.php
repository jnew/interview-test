<section class="homeFeature">
  <div class='row'>
    <div class="large-8 columns large-centered">
      <div class="searchContainer">
        <h2>Find your home</h2>
          <?php get_template_part('elements/searchBox');?>
      </div>
    </div>
  </div>
</section>
