<section class="tweets">
    <h2 class="staticHeading">Tweets</h2>
    <div class="tweetsContainer">
      <div class="row">
        <div class="large-12 tweetsSlider columns">
          <?php
            $tweets = getTweets(9, 'bbcbreaking');
          ?>
          <?php foreach($tweets as $tweet):?>
            <?php
              $time = $tweet['created_at'];
              $text = $tweet['text'];
            ?>
            <div class="tweet">
              <div class="tweetInner">
                <h6>BBC News</h6>
                <div class="tweetInfo fw300">@BBCbreaking</div>
                <div class="tweetInfo">
                  <?php echo $time;?>
                </div>
                <div class="tweetInfo tweetText">
                    <?php echo $text;?>
                </div>
              </div>
            </div>
          <?php endforeach;?>
        </div>
      </div>
    </div>
</section>
