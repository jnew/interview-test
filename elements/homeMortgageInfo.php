<section class="housingInfo">
    <div class="fullWidthRow row"  data-equalizer>
      <div class="large-6 medium-6 columns contentMenu"  data-equalizer-watch>
        <ul>
          <li><a class="active" href="">Register with us </a><i class="fa fa-chevron-left rotateRight"></i></li>
          <li><a href="">Mortgage Calculator </a><i class="fa fa-chevron-left"></i></li>
          <li><a href="">How Shared Ownership works </a><i class="fa fa-chevron-left"></i></li>
        </ul>
      </div>
      <div class="large-6 medium-6 columns contentContainer" data-equalizer-watch>
        <div class="content active">
          <h4>Register with us</h4>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
            ex ea commodo consequat. Duis aute irure dolor in.
          </p>
          <a class="ghostButton greenBorderButton">
            Register Now
          </a>

        </div>
        <div class="content">
          <h4>Mortgage Calculator</h4>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
            ex ea commodo consequat. Duis aute irure dolor in.
          </p>
          <a class="ghostButton greenBorderButton">
            Button Text
          </a>

        </div>
        <div class="content">
          <h4>How shared ownership works</h4>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
            ex ea commodo consequat. Duis aute irure dolor in.
          </p>
          <a class="ghostButton greenBorderButton">
            Button Text
          </a>

        </div>
      </div>
    </div>

</section>
