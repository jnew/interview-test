<section class="housingCategories">
  <div class="row">
    <div class="large-4 medium-4 columns housingCategory">
        <div class="inner">
          <?php $housing = get_post(19);?>
          <h6><?php echo $housing->post_title;?></h6>
          <div class="housingCategoryDescription">
            <h5><?php echo get_field('subtitle',19);?></h5>
            <?php echo $housing->post_content;?>
          </div>
        </div>
    </div>
    <div class="large-4 medium-4 columns housingCategory">
      <div class="inner">
          <?php $renting = get_post(20);?>
          <h6><?php echo $renting->post_title;?></h6>
          <div class="housingCategoryDescription">
            <h5><?php echo get_field('subtitle',20);?></h5>
              <?php echo $renting->post_content;?>
          </div>
      </div>
    </div>
    <div class="large-4 medium-4 columns housingCategory">
      <div class="inner">
          <?php $developments = get_post(21);?>
          <h6><?php echo $developments->post_title;?></h6>
          <div class="housingCategoryDescription">
            <h5><?php echo get_field('subtitle',21);?></h5>
            <?php echo $developments->post_content;?>
          </div>
      </div>
    </div>
  </div>
</section>
