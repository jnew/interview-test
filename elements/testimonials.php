<section class="testimonials">
  <h2 class="staticHeading">What our customers think</h2>
  <div class="testimonialsContainer">
    <div class="row">
      <div class="large-12 columns">
          <div class="testimonialSlider">
          <?php $testimonials = new WP_Query(array('post_type' => 'testimonials','showposts' => 3)); ?>
          <?php $i = 0;?>
          <?php while( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
            <div class="testimonial">
              <div class="inner">

                  <?php the_content();?>
                  <h6><?php the_title();?></h6>
              </div>

            </div>
          <?php endwhile;?>
          <?php wp_reset_query();?>
          </div>
      </div>
    </div>

  </div>
</section>
