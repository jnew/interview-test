<section class="newsUpdates">
  <h2 class="staticHeading">Latest Updates</h2>
  <?php $newsPosts = new WP_Query(array('showposts' => 3 ,'order' => 'ASC')); ?>
  <?php $i = 0;?>
  <?php while( $newsPosts->have_posts() ) : $newsPosts->the_post(); ?>
    <div class="newsUpdatesPost" style="">
        <div class="row">
          <div class="large-12 columns">
              <?php if($i % 2 == 0):?>
              <!-- Image Left -->
              <div class="newsContentContainer contentLeft">
              <?php else:?>
              <div class="newsContentContainer ">
              <?php endif;?>
                <?php
                  $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
                  if(empty($image)){
                    $image = 'http://lorempixel.com/450/360/city';
                  }
                ?>
                <img src="<?php echo $image;?>" />
                <div class="newsContent">
                  <a class="readMore">Read More</a>
                  <div class="inner">
                    <h5><?php the_title();?></h5>
                    <?php the_excerpt();?>
                  </div>

                </div>
              </div>
            </div>
          </div>
    </div>
  <?php $i++;?>
  <?php endwhile;?>
  <?php wp_reset_query();?>
</section>
