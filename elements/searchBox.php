<div class="searchBox">
  <form>
    <input type="text" placeholder="Enter location" class="locationField" name="location" />
    <input type="text" placeholder="1 Room" class="roomsField" value="1 Room" name="rooms" />
    <div class="buttons">
      <div class="incrementButtons">
        <span class="plus incrementButton">+</span>
        <span class="minus incrementButton">-</span>
      </div>

      <input type="submit" value="GO" />
    </div>

  </form>
</div>
