<section class="socialBlock">
  <div class='row'>
    <div class='large-8 columns large-centered'>
      <h3>Connect With Us</h3>
      <ul class="socialList">
        <li><a href=""><i class="fa fa-facebook"></i></a></li>
        <li><a href=""><i class="fa fa-twitter"></i></a></li>
        <li><a href=""><i class="fa fa-youtube-play"></i></a></li>
        <li><a href=""><i class="fa fa-envelope"></i></a></li>
        <li><a href=""><i class="fa fa-phone"></i></a></li>
      </ul>
    </div>
  </div>

</section>
