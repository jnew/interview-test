<?php /*
Template Name: Home-Page
*/ get_header(); ?>
<?php get_template_part('elements/homeFeature');?>
<?php get_template_part('elements/housingCategories');?>
<?php get_template_part('elements/homeMortgageInfo');?>
<?php get_template_part('elements/newsUpdates');?>
<?php get_template_part('elements/tweets');?>
<?php get_template_part('elements/testimonials');?>
<?php get_template_part('elements/socialBlock');?>
<?php get_footer(); ?>
