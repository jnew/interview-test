<?php get_header(); ?>

<?php /* if ( is_active_sidebar( 'sidebar' ) ) : ?>
	<?php dynamic_sidebar( 'sidebar' ); ?>
<?php endif; */ ?>

<section id="search">
<?php if ( have_posts() ) : ?>
	<h1>Search Results for &quot;<?php echo get_search_query(); ?>&quot;</h1>
	<?php while ( have_posts() ) : the_post(); ?>
	<div class="post-<?php the_ID();?>">
		<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
		<a href="<?php the_permalink() ?>"><?php echo get_excerpt_by_id($post_id); ?></a>
	</div>
	<?php endwhile; ?>
	<div class="page_nav">
		<?php previous_posts_link('Newer Posts') ?>
		<?php next_posts_link('Older Posts') ?>
	</div>
<?php else: ?>
	<h2>Search Results for &quot;<?php echo get_search_query(); ?>&quot;</h2>
	<div class="post-none">
		<p>Sorry, no posts matched &quot;<?php echo get_search_query(); ?>&quot;. Why not try browsing the site?</p>
<?php endif; ?>
</section>

<?php get_footer(); ?>